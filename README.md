### Hi! Candidate. ##
#### This is your assignment, please follow task description. ####


### Task description and rules ###

For this task You should use ReactJS together with jsx syntax. The best mobile experience is very important for us, but we also care about tablet and desktop users.
You are free to use tools that will help you to complete this task. We would like to understand your perfect workflow. Please include all important source files in your repository.
There is no design specification, we assume that you can use common UI patterns to complete this task. You will get more points if you can style this app without any external libs like Bootstrap. Feel free to resolve all UX challenges by yourself.

### Let's begin. ###
1. Create an array with list of heroes that has specific properties: "id", "name", "experience", "skill" and "image".
2. Your application should render the list of top 10 users with highest experience level.
3. User should be able to increase or decrease level of experience by clicking arrows next to the value of the field.
4. User can delete hero by clicking „x” in the last column.
5. Input form that will add values to the list should be provided. All fields should be required by default except the image field. Images can be a links to external resources in this case.
6. Create search field that can filter the list by hero name or skill name. If there is no results, please provide adequate information.

### How to submit this task? ###

It's easy, just create bitbucket repository (private) and grant access to jakub_kwasek

### How much time this task should take ###

We assume that 5 days should be enough for this task, but we can wait a bit longer for a good quality project.


### Questions? ###

For any technical questions, please contact: jakub.kwasek@stepstone.com

Good luck!